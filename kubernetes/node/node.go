package node

import (
	commonMgo "gitlab.com/wmf508/alfred/database/mongodb"
)

type Node struct {
	commonMgo.CommonField
	Hostname   string `bson:",omitempty"`                               // 主机名
	InternalIp string `json:"internal_ip" bson:"internal_ip,omitempty"` // 节点IP地址, 如：192.168.1.2
	Status     int    `bson:",omitempty"`                               // 节点状态, 1 Ready; 2 NoReady; 3 Unknown
}

const (
	StatusReady   = 1
	StatusNoReady = 2
	StatusUnknown = 3
)

const (
	FieldInternalIp = "internal_ip"
	FieldHostname   = "hostname"
	FieldStatus     = "status"
)
