package log

import (
	"gitlab.com/wmf508/alfred/log"
)

type Logger = log.Logger

func GetLogger() Logger {
	logger := log.GetLogger()
	return logger
}
