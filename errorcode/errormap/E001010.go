package errormap

import "gitlab.com/wmf508/alfred/errorcode/errormap"

var E001010 = map[string]*errormap.ErrorDesc{
	"E001010001": {
		ErrorCode: "E001010001",
		LogMsg:    "failed to create main server",
		UserMsg:   "",
	},
}
