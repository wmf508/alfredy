package errorcode

import (
	commonErrCode "gitlab.com/wmf508/alfred/errorcode"
	commonErrMap "gitlab.com/wmf508/alfred/errorcode/errormap"
	"gitlab.com/wmf508/alfredy/errorcode/errormap"
)

type ErrorCode struct{}

func GetErrorCode() *ErrorCode {
	return &ErrorCode{}
}

func (errCode *ErrorCode) GetDesc(code string) (*commonErrMap.ErrorDesc, error) {
	return commonErrCode.BasicGetDesc(code, errormap.ProjectMap)
}
