package fdp

import (
	"gitlab.com/wmf508/alfredy/log"
)

type Fdp struct {
	Log log.Logger
}

func GetFdp() *Fdp {
	return &Fdp{}
}
