module gitlab.com/wmf508/alfredy

go 1.17

require go.mongodb.org/mongo-driver v1.8.4

require gitlab.com/wmf508/alfred v0.0.0-20220412151644-6af722e10290

require (
	github.com/go-stack/stack v1.8.0 // indirect
	github.com/golang/snappy v0.0.1 // indirect
	github.com/klauspost/compress v1.13.6 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/xdg-go/pbkdf2 v1.0.0 // indirect
	github.com/xdg-go/scram v1.0.2 // indirect
	github.com/xdg-go/stringprep v1.0.2 // indirect
	github.com/youmark/pkcs8 v0.0.0-20181117223130-1be2e3e5546d // indirect
	golang.org/x/crypto v0.0.0-20201216223049-8b5274cf687f // indirect
	golang.org/x/sync v0.0.0-20201207232520-09787c993a3a // indirect
	golang.org/x/text v0.3.6 // indirect
)

require (
	github.com/google/go-cmp v0.5.5 // indirect
	github.com/stretchr/testify v1.7.1 // indirect
	gitlab.com/wmf508/ghost-rider v0.0.0-20220406142910-5f3364dcbf52
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b // indirect
)
